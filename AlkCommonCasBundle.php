<?php

namespace Alk\Common\CasBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Alk\Common\CasBundle\DependencyInjection\Security\Factory\CasFactory;

/**
 * Bundle base class.
 * 
 * @author alkante <support@alkante.com>
 */
class AlkCommonCasBundle extends Bundle
{

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\HttpKernel\Bundle\Bundle::build()
     */
    public function build(ContainerBuilder $container)
    {
        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new CasFactory());
    }

}

