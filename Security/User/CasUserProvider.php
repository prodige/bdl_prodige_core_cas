<?php

namespace Alk\Common\CasBundle\Security\User;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author alkante <support@alkante.com>
 */
class CasUserProvider implements UserProviderInterface {
    
    public function loadUserByUsername($username)
    {
        return new User(null, $username, null, array('ROLE_USER'));
    }

    public function refreshUser(UserInterface $user)
    {
        // if user has been reloaded from the session, just return it...
        return $user;
    }

    public function supportsClass($class)
    {
        return $class === 'Alk\Common\CasBundle\Security\User\User';
    }
    
}
