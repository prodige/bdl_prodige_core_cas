<?php

namespace Alk\Common\CasBundle\Security\Authentication\Provider;

use Exception;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Alk\Common\CasBundle\Security\Authentication\Token\CasUserToken;
use Alk\Common\CasBundle\Security\User\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Alk\Common\CasBundle\Event\AuthenticateUserEvent;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestMatcher;

/**
 * CAS Authentication provider.
 *
 * @author alkante <support@alkante.com>
 */
class CasProvider implements AuthenticationProviderInterface
{
    use ContainerAwareTrait;

    private static $_container;


    private $userProvider;
    private $cacheDir;
    private $requestStack;
    private $requestMatcher;

    private $debug;

    private $pattern;
    private $proxy;
    private $cas_host;
    private $cas_context;
    private $cas_port;
    private $cas_force_auth;
    private $cas_version;
    private $cas_server_ca_cert_path;
    private $cas_server_validation;
    private $cas_curl_options;

    /**
     * @param UserProviderInterface $userProvider
     * @param mixed $cacheDir
     * @param RequestStack $requestStack
     * @param string $pattern Request pattern that matches this CAS provider
     * @param $proxy
     * @param string $cas_host Full Hostname of your CAS Server.
     * @param string $cas_context Context of the CAS Server.
     * @param string $cas_port Port of your CAS server. Normally for a https server it's 443.
     * @param string $cas_force_auth Force authentication if true (redirect), or check authentication if false (no redirect)
     * @param string $cas_version CAS Protocol version, ie. CAS_VERSION_X_0
     * @param string $debug Activate \phpCAS debug mode ?
     * @param string $cas_server_ca_cert_path Path to the ca chain that issued the cas server certificate.
     * @param string $cas_server_validation Enable SSL server validation ?
     * @param array $cas_curl_options Array of curl options (e.g. CURLOPT__XXX)
     */
    public function __construct(
        UserProviderInterface $userProvider,
        $cacheDir,
        RequestStack $requestStack,
        $pattern,
        $proxy,
        $cas_host,
        $cas_context,
        $cas_port,
        $cas_force_auth,
        $cas_version,
        $debug,
        $cas_server_ca_cert_path,
        $cas_server_validation,
        $cas_curl_options
    ) {
        $this->userProvider = $userProvider;
        $this->cacheDir = $cacheDir;
        $this->requestStack = $requestStack;
        $this->requestMatcher = new RequestMatcher($pattern);

        $this->debug = $debug;

        $this->pattern = $pattern;
        $this->proxy = $proxy;
        $this->cas_host = $cas_host;
        $this->cas_context = $cas_context;
        $this->cas_port = intval($cas_port);
        $this->cas_force_auth = $cas_force_auth;
        $this->cas_version = $cas_version;
        $this->cas_server_ca_cert_path = $cas_server_ca_cert_path;
        $this->cas_server_validation = $cas_server_validation;
        $this->cas_curl_options = $cas_curl_options;
    }

    /**
     * @param TokenInterface $token
     * @return CasUserToken
     * @see \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface::authenticate()
     */
    public function authenticate(TokenInterface $token)
    {

        $logfile = str_replace("/app", "/var", $this->container->get('kernel')->getLogDir()).'/phpCas.log';
        if (true === $this->debug || 'true' === $this->debug) {
            \phpCAS::setDebug($logfile);
        }

        // Initialize \phpCAS - choose only one of client or proxy
        if (true === $this->proxy) {
            \phpCAS::proxy($this->cas_version, $this->cas_host, $this->cas_port, $this->cas_context);
            if ($this->container->hasParameter('cas_callback_url')) {
                $request = $this->container->get('request_stack')->getCurrentRequest();
                \phpCAS::setFixedCallbackURL(
                    $request->getSchemeAndHttpHost().$request->getBaseUrl().$this->container->getParameter(
                        'cas_callback_url'
                    )
                );
            }

            // Allow this client to be proxied
            if ($this->container->hasParameter('cas_proxy_chain')) {
                $proxy_chain = $this->container->getParameter('cas_proxy_chain');
                if (is_array($proxy_chain)) {
                    \phpCAS::allowProxyChain(new \CAS_ProxyChain($proxy_chain));
                } else {
                    \phpCAS::allowProxyChain(new \CAS_ProxyChain_Any());
                }
            } else {
                \phpCAS::allowProxyChain(new \CAS_ProxyChain_Any());
            }
        } else {
            \phpCAS::client($this->cas_version, $this->cas_host, $this->cas_port, $this->cas_context);
        }


        // For production use set the CA certificate that is the issuer of the cert
        // on the CAS server and uncomment the line below
        if ($this->cas_server_ca_cert_path != '') {
            \phpCAS::setCasServerCACert($this->cas_server_ca_cert_path);
        }

        // For quick testing you can disable SSL validation of the CAS server.
        // THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION.
        // VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL!
        if (
            false === $this->cas_server_validation ||
            'false' === $this->cas_server_validation
        ) {
            \phpCAS::setNoCasServerValidation();
        }

        foreach ($this->cas_curl_options as $option) {
            if (defined($option['name'])) {
                \phpCAS::setExtraCurlOption(constant($option['name']), $option['value']);
            }
        }

        // enable single sign out capabilities
        \phpCAS::handleLogoutRequests(false);

        // logout if desired
        if (isset($_REQUEST['caslogout'])) {
            //\phpCAS::logout();
            $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $parts = parse_url($url);
            parse_str($parts['query'], $query); //grab the query part
            unset($query['caslogout']); //remove a parameter from query
            $dest_query = http_build_query($query); //rebuild new query
            if( isset($_SERVER["HTTP_X_FORWARDED_SERVER"]) ) {
                $protocol =  $_SERVER["HTTP_X_FORWARDED_PROTO"];
            } else {
                $protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http");
            }
            $dest_url = $protocol.'://'.$parts['path'].'?'.$dest_query; //add query to host
            \phpCAS::logoutWithRedirectService($dest_url);
        }
        // is the user authenticated ?
        $authenticated = false;

        if (true === $this->cas_force_auth) {
            // if user is not authenticated and we're coming from an Ajax call -> throw exception
            if ($this->requestStack->getCurrentRequest()->isXmlHttpRequest() && false === \phpCAS::isAuthenticated()) {
                throw new AuthenticationException('(XmlHttpRequest) User is not authenticated.');
            }
            // force CAS authentication
            \phpCAS::forceAuthentication();
            $authenticated = true;
        } else {
            //TODO Valid this update
            //since casuser is not yet a parameter, force auth when connect is passed
            $request = $this->container->get('request_stack')->getCurrentRequest();
            if ($request->attributes->get('_route') == "auth_connect") {
                $authenticated = \phpCAS::checkAuthentication();

                // test if Proxy ticket is still valid calling cas, since logout does not destroy session correctly
                // force auth in this case. (may be removed when upgrading php/cas ??)
                if(isset($_SESSION['phpCAS']['pgt']) && !$this->getPGT($this->container->getParameter('PRODIGE_URL_ADMINCARTO').'/')){
                    unset($_SESSION['phpCAS']['pgt']);
                    $authenticated = \phpCAS::checkAuthentication();
                }
                

            } else {
                $authenticated = \phpCAS::isAuthenticated();
            }
        }

        try {

            // IS_AUTHENTICATED_ANONYMOUSLY
            $roles = array($authenticated ? 'ROLE_USER' : 'IS_AUTHENTICATED_ANONYMOUSLY');

            // build a new token with the authenticated user
            $authenticatedToken = new CasUserToken($roles);
            $authenticatedToken->setUser(new User(0, 'default', null, $roles));

            $authenticatedToken->setAuthenticated($authenticated);

            $dispatcher = $this->container->get('event_dispatcher');
            $attributes = array_merge(
                $authenticated ? \phpCAS::getAttributes() : array(),
                array('usr_id' => $authenticated ? \phpCAS::getUser() : null)
            );
            $event = new AuthenticateUserEvent($authenticatedToken, $attributes);
            $dispatcher->dispatch($event, AuthenticateUserEvent::EVENT_NAME);

            $user = $event->getUser();
            $exception = $event->getException();

            if ($exception) {
                throw $exception;
            }

            if ($user) {
                $authenticatedToken->setUser($user);
                $authenticatedToken->setAuthenticated(true);
            }

            return $authenticatedToken;
        } catch (Exception $ex) {
            throw new AuthenticationException($ex->getMessage());
        }
    }

    /**
     * Retreive a Proxy Granting Ticket for a specific service url
     * @param string $service the service to be proxied
     * @return mixed the ticket, or false
     * @throws Exception
     */
    public static function getPGT($service)
    {
        $err_code = $err_msg = null;
        try {
            return \phpCAS::retrievePT($service, $err_code, $err_msg);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param ContainerInterface $container
     * @see ContainerAwareTrait
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        self::$_container = $container;
    }

    /**
     * @param TokenInterface $token
     * @return bool
     * @see \Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface::supports()
     */
    public function supports(TokenInterface $token)
    {
        return $token instanceof CasUserToken && $this->requestMatcher->matches(
                $this->requestStack->getCurrentRequest()
            );
    }
}
