<?php

namespace Alk\Common\CasBundle\Security\Firewall;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Alk\Common\CasBundle\Security\Authentication\Token\CasUserToken;

/**
 * CAS Authentication listener.
 *
 * @author alkante <support@alkante.com>
 */
class CasListener
{
    protected $tokenStorage;
    protected $authenticationManager;
    protected $container;

    /**
     * Builds a new CasListener.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param AuthenticationManagerInterface $authenticationManager
     * @param ContainerInterface $container
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager, ContainerInterface $container)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
        $this->container = $container;
    }

    /**
     * @param RequestEvent $event
     */
    public function __invoke(RequestEvent $event)
    {
        $status  = Response::HTTP_FORBIDDEN;
        $message = $exception = null;
        try {
            $authToken = $this->authenticationManager->authenticate(new CasUserToken());
            $this->tokenStorage->setToken($authToken);

            return;
        } catch (AuthenticationException $failed) {
            $status    = Response::HTTP_UNAUTHORIZED;
            $exception = $failed;
            $message   = $failed->getMessage();
        }

        // By default deny authorization
        $response = new Response($message);
        $response->setStatusCode($status);
        $event->setResponse($response);
    }
}
