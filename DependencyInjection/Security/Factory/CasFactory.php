<?php

namespace Alk\Common\CasBundle\DependencyInjection\Security\Factory;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;

/**
 * Factory responsible for the definition and configuration of CAS Listener and Provider.
 *
 * @author alkante <support@alkante.com>
 */
class CasFactory implements SecurityFactoryInterface
{
    /**
     * (non-PHPdoc)
     * @see \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface::create()
     */
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $providerId = 'security.authentication.provider.cas.'.$id;
        $container
          ->setDefinition($providerId, new ChildDefinition('alk_common_cas.security.authentication.provider'))
          ->replaceArgument(0, new Reference($userProvider))
          ->replaceArgument(3, $config['pattern'])
          ->replaceArgument(4, $config['proxy'])
          ->replaceArgument(5, $config['host'])
          ->replaceArgument(6, $config['context'])
          ->replaceArgument(7, $config['port'])
          ->replaceArgument(8, $config['force_auth'])
          ->replaceArgument(9, $config['version'])
          ->replaceArgument(10, $config['debug'])
          ->replaceArgument(11, $config['server_ca_cert_path'])
          ->replaceArgument(12, $config['server_validation'])
          ->replaceArgument(13, $config['curl_options'])
          ;

        $listenerId = 'security.authentication.listener.cas.'.$id;
        $container->setDefinition($listenerId, new ChildDefinition('alk_common_cas.security.authentication.listener'));

        return array($providerId, $listenerId, $defaultEntryPoint);
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface::getPosition()
     */
    public function getPosition()
    {
        return 'pre_auth';
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface::getKey()
     */
    public function getKey()
    {
        return 'cas';
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface::addConfiguration()
     */
    public function addConfiguration(NodeDefinition $node)
    {
        // force phpCAS autoloading to access constants
        \phpCAS::getVersion();
        //\Alk\Common\CasBundle\Security\Authentication\Provider\phpCAS::getVersion();
        
        $node
          ->children()
          ->scalarNode('pattern')->defaultValue('^/')->end()
          ->scalarNode('proxy')->defaultFalse()->end()
          ->scalarNode('host')->defaultValue('localhost')->end()
          ->scalarNode('context')->defaultValue('context')->end()
          ->scalarNode('port')->defaultValue(433)->end()
          ->scalarNode('force_auth')->defaultTrue()->info('set to true to force authentication (redirect to cas), false to continue as an anonymous unauthenticated user')->end()
          ->scalarNode('version')->defaultValue(CAS_VERSION_3_0)->end()
          ->scalarNode('debug')->defaultFalse()->end()
          ->scalarNode('server_ca_cert_path')->defaultValue('')->end()
          ->scalarNode('server_validation')->defaultTrue()->end()
          ->arrayNode('curl_options')
            ->prototype('array')
              ->children()
                ->scalarNode('name')->end()
                ->scalarNode('value')->end()
              ->end()
            ->end()
          ->end()
          ;
    }

}