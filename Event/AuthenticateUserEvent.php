<?php

namespace Alk\Common\CasBundle\Event;

use Exception;
use Symfony\Contracts\EventDispatcher\Event;
use Symfony\Component\Security\Core\User\UserInterface;
use Alk\Common\CasBundle\Security\User\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * @author alkante <support@alkante.com>
 */
class AuthenticateUserEvent extends Event
{
    
    const EVENT_NAME = 'alk.common.cas.authenticateuser';
    
    private $token;
    
    private $attributes;
    
    private $user;
    
    private $exception;

    /**
     * @param TokenInterface $token
     * @param array $attributes
     */
    public function __construct(TokenInterface $token, Array $attributes=array())
    {
        $this->token      = $token;
        $this->attributes = $attributes;
    }
    
    /**
     * @param UserInterface $user
     * @param Exception $e
     */
    public function setUser(UserInterface $user=null, Exception $e=null)
    {
        $this->user = $user;
        $this->exception = $e;
    }
    
    /**
     * 
     * @return TokenInterface
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Exception
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * @param Exception $exception
     * @return Exception
     */
    public function setException(Exception $exception)
    {
        $this->exception = $exception;
        
        return $this;
    }

    /**
     * Instanciates a default User
     * @param string $id
     * @param string $username
     * @param string $password
     * @return User
     */
    public function createDefaultUser($id, $username, $password)
    {
        return new User($id, $username, $password, array('ROLE_USER'));
    }
    
    /**
     * Instanciates a default anonymous User
     * @param string $id
     * @param string $username
     * @param string $password
     * @return User
     */
    public function createAnonymoustUser($id, $username, $password)
    {
        return new User($id, $username, $password, array('IS_AUTHENTICATED_ANONYMOUSLY'));
    }
    
}
